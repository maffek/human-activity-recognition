# Human Activity Recognition
11@KSA2020
Things to do in project: 
1. Analyze the documentation and current literature 
   - Theoretical research 
2. Discuss and choose the way of obtaining new data 
3. Setting up a way to connect the phone/sensors to computational center (PC probably) 
4. Find a way to process data to the similiar state in which data from UCI is 
5. Prepare machine learning model from data from UCI
6. Test different kinds of models 
    - Number of layers 
    - Type of neural network 
    - Activation function 
    - Optymalizer 
-----------------------------------------------------------------------------------------------------------------------
SUMMER (hopefully) 

-----------------------------------------------------------------------------------------------------------------------
7. Setting up a number of ideal activities 
8. Collect data from volunteers 
9. General testing 
10. Improvement 
    - GUI 
    - Time response 
    - Accuracy 
11. Documentation